﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerItems : MonoBehaviour {

	// Use this for initialization




    //7 ItemId slots, E use, Q throw, 



    //public List<GameObject> items;

    public GameObject[] items;


    Sensor pickSensor;
    Transform sideManage;
    Transform holdPos;

    int inventoryLimit = 7;
    int inventoryIndex = 0;

    GameObject currentItem;

    bool toss = false;


    float tossTime = 0;
    float maxTossTime = 2;

    public delegate void UseItem();

    public UseItem use;

    RawImage pwrBar;
    float maxTossPower = 40;

    float dirX;
    float dirY;
    float face;
    Color uiHide;
    Color pwrBarColour;



    void Awake () {
        uiHide = new Color(1, 1, 1, 0);
        sideManage = transform.FindChild("SideManager");
        holdPos = sideManage.FindChild("HoldPos");
        //items = new List<GameObject>();

        items = new GameObject[inventoryLimit];

        pickSensor = sideManage.FindChild("PickUpSensor").GetComponent<Sensor>();

        pwrBar = GameObject.FindGameObjectWithTag("ThrowBar").GetComponent<RawImage>();

        pwrBarColour = pwrBar.color;
        pwrBar.color = uiHide;

	}
	
	// Update is called once per frame
	void Update () {



        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        //Debug.Log(h);



        if (h > 0.1f)
        {
            dirX = 1;
            face = 1;
        }
        else if (h < -0.1f)
        {
            dirX = -1;
            face = -1;
        }
        else
        {
            dirX = 0;
        }


        if (v > 0.1f)
        {
            dirY = 1;
        }
        else if (v < -0.1f)
        {
            dirY = -1;
        }
        else
        {
            dirY = 0;
        }















        if(Input.GetButtonDown("PickUp"))
        {
            if(currentItem==null)
            {
                if (pickSensor.detect)
                {

                    Debug.Log("Pick up " + pickSensor.collideData.name);
                    GameObject tmp = pickSensor.collideData.gameObject;

                    currentItem = tmp;
                    items[inventoryIndex] = tmp;
                    tmp.transform.SetParent(holdPos);
                    tmp.transform.position = holdPos.position;
                    tmp.transform.rotation = Quaternion.identity;
                    tmp.GetComponent<Rigidbody2D>().isKinematic = true;
                    tmp.layer = LayerMask.NameToLayer("HeldItem");
                    tmp.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.None;



                    if(currentItem.CompareTag("Donut"))
                    {
                        use = Donut;
                    }

                }
            }
            else
            {
                Debug.Log("Use "+currentItem.tag);

                use();



            }
            
            
            
           
        
        
        
        
        }


        if (Input.GetButtonDown("Throw"))
        {
            if(currentItem!=null)
            {
                Debug.Log("THROW");
                toss = true;
                pwrBar.color = pwrBarColour;
            }



        }


        if(toss)
        {
            tossTime =Mathf.Clamp(tossTime+Time.deltaTime,0,maxTossTime);

            float powerPercent = (tossTime / maxTossTime) * 100;


            Vector2 size = pwrBar.rectTransform.sizeDelta;

            size.x = 100 - powerPercent;

            pwrBar.rectTransform.sizeDelta = size;

        }


        if(Input.GetButtonUp("Throw"))
        {

            if(toss)
            {
                
  



                float powerPercent = tossTime / maxTossTime;
                float power = powerPercent * maxTossPower;

                Vector2 t = Vector2.zero;


                if (dirY == 1)
                {
                    if (face == 1)
                    {
                        t.Set(0.2f, 1f);
                    }
                    else
                    {
                        t.Set(-0.2f, 1f);
                    }
                }
                else
                {
                    t.Set(face, 0.15f);
                }





                holdPos.DetachChildren();

                currentItem.layer = LayerMask.NameToLayer("Pickup");



                //Vector3 tp = heldItem.transform.position;

                //tp.z = 0;

                //transform.position = tp;

                currentItem.GetComponent<Rigidbody2D>().isKinematic = false;
                //Add current velocity to the pickup so it is always being thrown forwards




                currentItem.GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity;
                currentItem.GetComponent<Rigidbody2D>().AddForce(t * power, ForceMode2D.Impulse);
                currentItem.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.Interpolate;
                Debug.Log(tossTime);

                tossTime = 0;

                toss = false;
                currentItem = null;


                Vector2 size = pwrBar.rectTransform.sizeDelta;

                size.x = 100;

                pwrBar.rectTransform.sizeDelta = size;
                pwrBar.color = uiHide;









            }


        }





	}

    public void Smoke()
    {

    }

    public void Donut()
    {
        Debug.Log("EAT DONUT");

        Destroy(currentItem);
        items[inventoryIndex] = null;
    }

}
