﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapManager : MonoBehaviour {

	// Use this for initialization


    public List<GameObject> cAreas = new List<GameObject>();

    public int currentArea;

    public bool changeArea = false;

    public GameObject destPoint;


    public float a1 = 0;
    public float a2 = 0;



    public Transform Z0;

    public bool dir;

    MoveController playerCon;



	void Awake () {


        playerCon = GameObject.FindGameObjectWithTag("Player").GetComponent<MoveController>();

        Z0 = GameObject.FindGameObjectWithTag("Z0").transform;

        cAreas.AddRange(GameObject.FindGameObjectsWithTag("Area"));


        //Transform t=transform.FindChild("AreaColliders");


        //foreach(Transform child in t)
        //{
        //    //Debug.Log(child.name);

        //    cAreas.Add(child.gameObject);

        //}


        //cAreas[1].SetActive(false);

        //Debug.Log(cAreas[1].name);



	}
	
	// Update is called once per frame
	void Update () {
	

        if(changeArea)
        {
            


            if(dir)
            {
                Vector3 p = transform.position;

                p.z = Mathf.Clamp(p.z + 5.5f * Time.deltaTime, 0, 100);


                transform.position = p;


                if (destPoint.transform.position.z >= 0)
                {
                    changeArea = false;
                    playerCon.inputLock = false;
                    playerCon.UnFreeze();
                }


                Debug.Log("D=" + destPoint.transform.position);
            }
            else
            {

                Vector3 p = transform.position;

                p.z = Mathf.Clamp(p.z - 5.5f * Time.deltaTime,0,100);


                transform.position = p;


                if(destPoint.transform.position.z<=0)
                {
                    changeArea = false;
                    playerCon.inputLock = false;
                    playerCon.UnFreeze();
                }


                Debug.Log("D=" + destPoint.transform.position);


            }





        }


        //if(changeArea)
        //{



        //    Vector3 tmp = transform.position;


        //    float z = Mathf.Lerp(a1, a2,Time.deltaTime);



        //    Debug.Log(z);
        //    //Vector3 temp



        //    //Debug.Log(g);




        //    //Vector3 temp = transform.position - destPoint;




        //    //temp.z = Mathf.Lerp(transform.position.z, temp.z, Time.deltaTime);
            
            
            
            
        //    ////temp.x = 0;
        //    //transform.position = temp;


        //    //Debug.Log(temp);


        //   // transform.position=Vector3.Lerp(transform.position, destPoint.localPosition, Time.deltaTime);



        //}

	}


    public void ChangeArea(GameObject g)
    {

        playerCon.inputLock = true;
        playerCon.Freeze();
        foreach(GameObject child in cAreas)
        {
            child.SetActive(false);
        }

        g.SetActive(true);

        //cAreas[id].SetActive(true);
        destPoint=g;


        


        changeArea = true;


        Vector3 d = Z0.position - g.transform.position;

        d.Normalize();

        Debug.Log("Z0-A=" + d);

        if(d.z>0)
        {
            dir = true;
        }
        else if(d.z<0)
        {
            dir = false;
        }


        //a1 = transform.position.z;
        //a2 = -destPoint.z;


    }


}
