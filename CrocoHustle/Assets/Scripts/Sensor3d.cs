﻿using UnityEngine;
using System.Collections;

public class Sensor3d : MonoBehaviour {

	// Use this for initialization

    public bool detect;
    public Collider collideData;

	void Awake () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (collideData != null && !collideData.enabled)
        {
            detect = false;
        }

	}



    void OnTriggerEnter(Collider c)
    {
        detect = true;
        collideData = c;


    }

    
    void OnTriggerStay(Collider c)
    {
        detect = true;
        collideData = c;
    }

    void OnTriggerExit(Collider c)
    {
        detect = false;

    }


}
