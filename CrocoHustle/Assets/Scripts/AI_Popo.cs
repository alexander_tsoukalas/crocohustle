﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MoveController))]
public class AI_Popo : MonoBehaviour {
    
    public float face = 1;
    public float dirX = 0;
    public float dirY = 0;
    public bool jump = false;

    public GameObject pointA;
    public GameObject pointB;

    public bool dPoint=true;


    public delegate void AiMode();


    AiMode mode;

    MoveController control;



	void Awake () {
        control = GetComponent<MoveController>();

        mode = Patrol;


	}
	
	// Update is called once per frame
	void Update () {
	

        if(pointA!=null&&pointB!=null)
        {
            mode();
        }


	}


    void FixedUpdate()
    {
        if(control.action!=null)
        {
            control.action(0, 0, false, false);
        }
    }


    void Patrol()
    {
        if(dPoint)
        {
            Vector2 len = pointA.transform.position - transform.position;

            float dis = len.sqrMagnitude;

            len.Normalize();


            if(len.x>0)
            {
                dirX=1;
            }
            else if(len.x<0)
            {
                dirX=-1;
            }

            if(dis<5*5)
            {
                Debug.Log("CLOSE");
                dirX = 0;
            }


            


            


            //if(len.sqrMagnitude<5*5)
            //{
            //    Debug.Log("Close!");
            //}

            //len.Normalize();
            //Debug.Log(len);

        }
        else
        {

        }
    }

    void Pursuit()
    {

    }

}
