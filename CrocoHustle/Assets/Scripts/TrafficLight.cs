﻿using UnityEngine;
using System.Collections;

public class TrafficLight : MonoBehaviour {

	// Use this for initialization


    GameObject redLight;

    GameObject greenLight;


    public GameObject syncLight;

    public GameObject controlArea;

    TrafficControlArea area;

    TrafficLight sync;

    bool playerIn = false;


    float timer = 0;
    float wait = 4;
    float greenDur = 10;


    public bool waitChange = false;

    bool isRed = true;


    bool hasPair = false;


    AudioSource clickFX;

	void Awake () {
        clickFX= GetComponent<AudioSource>();
        redLight=transform.Find("RedLight").gameObject;

        greenLight = transform.Find("GreenLight").gameObject;

        greenLight.SetActive(false);

        if(syncLight!=null)
        {
            hasPair=true;
            sync = syncLight.GetComponent<TrafficLight>();
        }

        if (controlArea!=null)
        {
            area = controlArea.GetComponent<TrafficControlArea>();
        }


	}
	
	// Update is called once per frame
	void Update () {


        if (playerIn && Input.GetButtonDown("PickUp"))
        {
            clickFX.PlayOneShot(clickFX.clip);

        }

        if(isRed)
        {
            //IF RED
            if (!waitChange)
            {
                if (playerIn && Input.GetButtonDown("PickUp"))
                {
                    Debug.Log("TURN GREEN!@");
                    waitChange = true;
                    area.pass = false;
                    if(hasPair)
                    {
                        sync.waitChange = true;
                    }

                }
            }
            else
            {
                timer += Time.deltaTime;
                if (timer >= wait)
                {

                    redLight.SetActive(false);
                    greenLight.SetActive(true);
                    isRed = false;
                    waitChange = false;
                    timer = 0;
                }
            }


        }
        else
        {
            //IF GREEN

            timer += Time.deltaTime;

            if(timer>=greenDur)
            {
                redLight.SetActive(true);
                greenLight.SetActive(false);
                isRed = true;
                area.pass = true;
                timer = 0;
            }




        }



     

	}


    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.CompareTag("Player"))
            playerIn = true;
        
    }

    void OnTriggerExit2D(Collider2D c)
    {
        if (c.CompareTag("Player"))
            playerIn = false;
    }

}
