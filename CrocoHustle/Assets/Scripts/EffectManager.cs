﻿using UnityEngine;
using System.Collections;

public class EffectManager : MonoBehaviour {

	// Use this for initialization


    Sensor body;

    Animator anim;
    public StatusEffect currentState;





    MoveController control;

    public Stats defaultStats;

	void Awake() {

        control = GetComponent<MoveController>();


        defaultStats = new Stats(control);




        anim = GetComponentInChildren<Animator>();
        body = transform.FindChild("BodyCheck").GetComponent<Sensor>();



	}
	
	// Update is called once per frame
	void Update () {
	

        if(body.detect)
        {
            if(body.collideData!=null)
            {
                
                if(body.collideData.CompareTag("Donut"))
                {
                    currentState = new FX_FAT(control);
                    Destroy(body.collideData.gameObject);
                }
             

        

                //if(body.collideData)
                //{

                //}
            }
        }
        else
        {
            
        }


        if(currentState!=null)
        {
            currentState.Update();

            if (currentState.deathMark)
            {
                Debug.Log("DIE FAT");
                currentState = null;
                control.SetStats(defaultStats);
                GetComponent<Rigidbody2D>().fixedAngle = true;
                GetComponent<Rigidbody2D>().MoveRotation(0);
                


            }
                

        }
        


	}
}

