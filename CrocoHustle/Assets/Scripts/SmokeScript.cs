﻿using UnityEngine;
using System.Collections;

public class SmokeScript : MonoBehaviour
{

    // Use this for initialization

    public float alive;
    public float alpha;
    public float alphaDecay;
    private float maxAlpha;

    public float force;

    private float myAlpha=0;

    public Vector2 direction;

    private float timer = 0;

    Animator anim;

    bool fadeOut = false;

    public bool activate = false;

    bool fadeIn = false;

    void Awake()
    {
        //maxAlpha = alpha;
        anim = GetComponent<Animator>();
        //anim.SetFloat("alpha", 0);
        

    }

    // Update is called once per frame
    void Update()
    {

        if (activate)
        {
            
            if(!fadeIn)
            {

                myAlpha = Mathf.Clamp(myAlpha + alphaDecay * Time.deltaTime, 0, maxAlpha);
                anim.SetFloat("alpha", myAlpha);

                //Debug.Log("A=" + myAlpha+"/"+maxAlpha);

                if(myAlpha==maxAlpha)
                {
                    fadeIn = true;
                }

            }
            else
            {
                if (!fadeOut)
                {
                    timer += Time.deltaTime;

                    if (timer >= alive)
                    {
                        //anim.SetBool("End", true);
                        fadeOut = true;
                    }
                }
                else
                {
                    myAlpha = Mathf.Clamp(myAlpha - alphaDecay * Time.deltaTime, 0, maxAlpha);
                    anim.SetFloat("alpha", myAlpha);

                    if (myAlpha == 0)
                    {
                        GameObject.Destroy(gameObject);
                    }

                }
            }


        }








        //timer += Time.deltaTime;

        //if(timer>=alive)
        //{
        //    anim.SetBool("End", true);
        //    Debug.Log("END");
        //}


        //Debug.Log("DERPAS:"+anim.GetCurrentAnimatorClipInfo(0).Length);
        //Debug.Log("T="+anim.playbackTime);
        //if (anim.GetCurrentAnimatorStateInfo(0).IsName("End"))
        //{
        //    // Avoid any reload.

        //    Debug.Log("");

        //}


    }

    public void Fire(float alive, float alpha, float force,float decay)
    {

        this.alive = alive;
        this.alpha = alpha;
        this.force = force;
        alphaDecay = decay;
        maxAlpha = alpha;

        GetComponent<Rigidbody2D>().AddForce(direction * force, ForceMode2D.Impulse);
        activate = true;
    }

}
