﻿using UnityEngine;
using System.Collections;

public class Sensor : MonoBehaviour {

    public bool detect = false;
    public Collider2D collideData;




    public void Update()
    {

        

        if(collideData!=null&&!collideData.isActiveAndEnabled)
        {
            detect = false;
        }
    }

  
    void OnTriggerEnter2D(Collider2D d)
    {
        detect = true;
        collideData = d;
    }

    void OnTriggerStay2D(Collider2D d)
    {
        detect = true;
        collideData = d;
    }

    void OnTriggerExit2D(Collider2D d)
    {
        detect = false;
        
    }
    
   
}
