﻿using UnityEngine;
using System.Collections;

public class FX_FAT : StatusEffect{

    MoveController m;
    float timer = 0;
    float maxTime = 10;
    Animator anim;
    public FX_FAT(MoveController m)
    {
        this.m = m;
        anim = m.anim;

        anim.SetBool("Fat", true);

    }



    public override void Update()
    {

        m.curMaxAccel = 5f;
        
        timer += Time.deltaTime;
        m.getRigidBody().fixedAngle = false;
        Debug.Log("FAT");


        if(timer>=maxTime)
        {
            this.deathMark = true;
            anim.SetBool("Fat", false);
        }

	}
}
