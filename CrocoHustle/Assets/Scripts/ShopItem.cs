﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ShopItem : MonoBehaviour {

	// Use this for initialization


    public bool playerDetect = false;

    Animator anim;

    public GameObject item;

    public string name;
    public int price;

    DuruManager duru;
    AudioSource mehFX;

    Text text;


    List<AudioSource> sounds = new List<AudioSource>();

	void Awake() {





        anim = GetComponent<Animator>();
        duru = GameObject.FindGameObjectWithTag("Player").GetComponent<DuruManager>();
        mehFX= GetComponent<AudioSource>();



        sounds.AddRange(transform.parent.GetComponents<AudioSource>());

        Debug.Log("SOUNDS#="+sounds.ToArray().Length);




        text = transform.Find("ItemName").GetComponent<Text>();

        text.text = name;
        text = transform.Find("ItemPrice").GetComponent<Text>();
        text.text = price + "$";
    }
	
	// Update is called once per frame
	void Update () {

        anim.SetBool("Detect", playerDetect);

        float lol = Input.GetAxis("PickUp");

        if(Input.GetButtonDown("PickUp")&&playerDetect)
        {

            Debug.Log("CRA! " + duru.duruNo+" "+price);
            if(duru.duruNo<price)
            {
               // mehFX.PlayOneShot(mehFX.clip);


                sounds[0].PlayOneShot(sounds[0].clip);

                anim.SetTrigger("Deny");
            }
            else
            {
                //sounds[1].PlayOneShot(sounds[1].clip);
                sounds[1].PlayOneShot(sounds[1].clip);
                Debug.Log("BUY!");

                Vector3 t = transform.position;

                t.z = 0;

                GameObject.Instantiate(item, t, Quaternion.identity);
                duru.duruNo = duru.duruNo - price;
                Destroy(gameObject);


            }

            
          
        }


	}


    void OnTriggerEnter2D(Collider2D c)
    {

        if (c.CompareTag("Player"))
            playerDetect = true;

    }

    void OnTriggerExit2D(Collider2D c)
    {
        if (c.CompareTag("Player"))
            playerDetect = false;
    }


}
