﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


[RequireComponent(typeof(MoveController))]
[RequireComponent(typeof(DuruManager))]
public class PlayerInput : MonoBehaviour {

    public MoveController controller;
    public float face = 1;
    public float dirX = 0;
    public float dirY = 0;
    public bool jump = false;
    public bool jumpBtn = false;

    public bool dash = false;
    public bool crouch = false;
    DuruManager duru;
    Transform jplace;
    Transform sideManage;

    Transform holdPos;

    public bool hasJ = false;


    float jTimer = 0;
    public float jDelay = 60;
    GameObject g;

    Sensor pickSensor;

    GameObject heldItem;


    bool isHolding = false;

    bool pickBtnDown = false;

    float throwTime = 0;
    bool startThrow = false;

    float maxThrowTime=2;
    float maxThrowPower = 40;

    RawImage pwrBar;

    Color uiHide;
    Color pwrBarColour;

    RawImage stmBar;

    RawImage breathBar;
    Color bColour;


    bool breathCh = false;
    float breathTime = 0;
    float maxBreathTime = 2;

    ParticleSystem breath;

    Sensor3d s3d;


    Animator anim;

    void Awake()
    {
        anim=GetComponentInChildren<Animator>();
        breath = GameObject.FindGameObjectWithTag("BreathAttack").GetComponent<ParticleSystem>();
        s3d = transform.Find("3DSensor").GetComponent<Sensor3d>();
        uiHide = new Color(1, 1, 1, 0);
        sideManage = transform.FindChild("SideManager");
        jplace = sideManage.FindChild("Jplace");
        pickSensor = sideManage.FindChild("PickUpSensor").GetComponent<Sensor>();
        controller = GetComponent<MoveController>();
        duru = transform.GetComponent<DuruManager>();

        holdPos = sideManage.FindChild("HoldPos");
        //pwrBar = GameObject.FindGameObjectWithTag("ThrowBar").GetComponent<RawImage>();


        //pwrBarColour=pwrBar.color;
        //pwrBar.color = uiHide;

        stmBar = GameObject.FindGameObjectWithTag("StaminaBar").GetComponent<RawImage>();
        breathBar = GameObject.FindGameObjectWithTag("BreathBar").GetComponent<RawImage>();
        bColour = breathBar.color;
        breathBar.color = uiHide;

    }


	void Update () {


            anim.SetBool("Flat",s3d.detect);
        



        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        
        //Debug.Log(h);



        if(h>0.1f)
        {
            dirX = 1;
            face = 1;
        }
        else if(h<-0.1f)
        {
            dirX = -1;
            face = -1;
        }
        else
        {
            dirX = 0;
        }


        if (v > 0.1f)
        {
            dirY = 1;
        }
        else if (v < -0.1f)
        {
            dirY = -1;
        }
        else
        {
            dirY = 0;
        }

        if(Input.GetButtonDown("Jump")&&!jumpBtn)
        {
            jumpBtn = true;
            jump = true;
        }

        if(Input.GetButtonUp("Jump"))
        {
            jumpBtn = false;
            //jump = false;
        }

        if(Input.GetButtonDown("Dash"))
        {
            dash = true;
        }

        if (Input.GetButtonUp("Dash"))
        {
            dash = false;
        }

        if(Input.GetButtonDown("Jay"))
        {
            Debug.Log("DURU:" + duru.duruNo);


            

            if(duru.duruNo>0&&!hasJ)
            {
                duru.duruNo--;
                g=Instantiate(Resources.Load("Jay"), jplace.transform.position, sideManage.rotation) as GameObject;

                g.transform.SetParent(jplace);


                hasJ = true;

            }


        }


        if(Input.GetButtonDown("Breath")&&hasJ)
        {
            breathCh = true;
            breathBar.color = bColour;
            breathBar.rectTransform.sizeDelta.Set(0, breathBar.rectTransform.sizeDelta.y);
        }


        if(breathCh)
        {
            breathTime = Mathf.Clamp(breathTime + Time.deltaTime, 0, maxBreathTime);


            float res = breathTime / maxBreathTime * 100;


            Debug.Log("%=" + res);


            Vector2 size = breathBar.rectTransform.sizeDelta;

            size.x =res;

            breathBar.rectTransform.sizeDelta = size;

            //breathBar.rectTransform.sizeDelta.Set(res, breathBar.rectTransform.sizeDelta.y);



            //if (breathTime >= maxBreathTime)
            //    Debug.Log("maxxed");
        }


        

        if(Input.GetButtonUp("Breath"))
        {
            breathCh = false;
            Debug.Log("BREATH=" + breathTime);
            breathTime = 0;
            breathBar.color = uiHide;

            breath.Play();
            //breathBar.rectTransform.sizeDelta.Set(0, breathBar.rectTransform.sizeDelta.y);
            
        }

        





        //How long the J will burn for
        if(hasJ)
        {
            jTimer += Time.deltaTime;

            if(jTimer>jDelay)
            {
                hasJ = false;
                Destroy(g);
                jTimer = 0;
                
            }
        }


        //Manage facing position
        if(face==1)
        {
            sideManage.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        }
        else if(face==-1)
        {
            sideManage.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
        }

        float lol = Input.GetAxis("PickUp");



        //Pickups & Throwing
        if ((Mathf.Abs(lol) > 0 || Input.GetButtonDown("PickUp"))&&!pickBtnDown)
        {

            pickBtnDown = true;
            

            if (!isHolding)
            {
                if (pickSensor.detect)
                {
                    //Debug.Log("PICKUP!");

                    //heldItem = pickSensor.collideData.gameObject;

                    //heldItem.transform.SetParent(holdPos);

                    //heldItem.transform.position = holdPos.position;
                    //heldItem.transform.rotation = Quaternion.identity;
                    //Debug.Log("" + heldItem.layer);
                    //heldItem.GetComponent<Rigidbody2D>().isKinematic = true;
                    //heldItem.layer = LayerMask.NameToLayer("HeldItem");
                    //heldItem.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.None;

                    //isHolding = true;
                }

            }
            else
            {
                Debug.Log("Throw");

                startThrow = true;

                pwrBar.color=pwrBarColour;
                //heldItem.transform.SetParent(null);
                //Vector3 t=heldItem.transform.position;
                //t.z = 0;
                //heldItem.transform.position=t;
                //heldItem.GetComponent<Rigidbody2D>().isKinematic = false;
                //heldItem.layer = LayerMask.NameToLayer("Pickup");
                //isHolding = false;

            }
        }

        //if(startThrow)
        //{
        //    throwTime = Mathf.Clamp(throwTime+Time.deltaTime,0,maxThrowTime);

        //    float powerPercent = (throwTime / maxThrowTime)*100;


        //    Vector2 size = pwrBar.rectTransform.sizeDelta;

        //    size.x = 100 - powerPercent;

        //    pwrBar.rectTransform.sizeDelta = size;

        //    //Debug.Log("PWRBAR="+pwrBar.rectTransform.sizeDelta);

        //}


        //if(Mathf.Abs(lol) == 0 || Input.GetButtonUp("PickUp"))
        //{
        //    pickBtnDown = false;

        //    if(startThrow)
        //    {

        //        float powerPercent = throwTime / maxThrowTime;
        //        float power = powerPercent * maxThrowPower;

        //        Vector2 t = Vector2.zero;


        //        if(dirY==1)
        //        {
        //            if (face == 1)
        //            {
        //                t.Set(0.2f, 1f);
        //            }
        //            else
        //            {
        //                t.Set(-0.2f, 1f);
        //            }
        //        }
        //        else
        //        {
        //            t.Set(face, 0.15f);
        //        }



                

        //        holdPos.DetachChildren();

        //        heldItem.layer = LayerMask.NameToLayer("Pickup");



        //        //Vector3 tp = heldItem.transform.position;

        //        //tp.z = 0;

        //        //transform.position = tp;

        //        heldItem.GetComponent<Rigidbody2D>().isKinematic = false;
        //        //Add current velocity to the pickup so it is always being thrown forwards




        //        heldItem.GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity;
        //        heldItem.GetComponent<Rigidbody2D>().AddForce(t * power, ForceMode2D.Impulse);
        //        heldItem.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.Interpolate;
        //        Debug.Log(throwTime);
        //        startThrow = false;
        //        throwTime = 0;
        //        isHolding = false;


        //        Vector2 size = pwrBar.rectTransform.sizeDelta;

        //        size.x = 100;

        //        pwrBar.rectTransform.sizeDelta = size;
        //        pwrBar.color = uiHide;
                
        //    }



        //}



        Vector2 tmp = stmBar.rectTransform.sizeDelta;


        tmp.x = controller.energy * 2;


        stmBar.rectTransform.sizeDelta = tmp;

        
	}


    void FixedUpdate()
    {
        //controller.Move(dirX,dirY,jump,dash);
        if(controller.action!=null&&!controller.inputLock)
        {
            controller.action(dirX, dirY, jump, dash);

            if(jump==true)
            {
                jump = false;
            }

        }
        

    }
}
