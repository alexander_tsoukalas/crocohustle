﻿using UnityEngine;
using System.Collections;

public class JayTest : MonoBehaviour
{

    // Use this for initialization

    public GameObject smoke;


    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("CLICK");

            GameObject s = Instantiate(smoke, transform.position, Quaternion.identity) as GameObject;

            SmokeScript scr = s.GetComponent<SmokeScript>();


            float alive = Random.Range(3, 5.5f);
            float alpha = Random.Range(0.5f, 0.95f);
            float force = Random.Range(2f, 5f);
            float alphaDecay = Random.Range(0.5f, 1.5f);

            float size = Random.Range(.5f, 1.5f);
            float yoff = Random.Range(-0.1f, 0.1f);

            scr.direction.Set(1, yoff);


            Debug.Log(size);
            s.transform.localScale = new Vector3(size, size, 1);


            scr.Fire(alive,alpha,force,alphaDecay);

        }

    }
}
