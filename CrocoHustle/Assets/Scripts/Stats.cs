﻿using UnityEngine;
using System.Collections;

[System.Serializable]

public class Stats  {


    public float maxAccel;
    public float accelInc;
    public float crouchSpeed;
    public float jumpForce;
    public float dashSpeed;
    public float energy;
    public float enCost;
    public float enRegen;
    public float maxEnergy;
    public bool jumpLock;
    public bool crouchLock;
    public bool dashLock;
    



    public Stats()
    {

    }


    public Stats(MoveController m)
    {


        maxAccel = m.curMaxAccel;
        accelInc = m.accelInc;
        crouchSpeed = m.crouchSpeed;
        jumpForce = m.jumpForce;
        dashSpeed = m.dashSpeed;
        energy = m.energy;
        enCost = m.enCost;
        enRegen = m.enRegen;
        maxEnergy = m.maxEnergy;

    }





}
