﻿using UnityEngine;
using System.Collections;

public class Doorway : MonoBehaviour {

	// Use this for initialization

    public int id=0;
    public bool player = false;

    public MapManager map;

    public float v;


    public GameObject destination;
    



    void Awake()
    {

        map = GameObject.FindGameObjectWithTag("Map").GetComponent<MapManager>();

    }



	// Update is called once per frame
	void Update () {



        if (destination != null)
        {
            v = Input.GetAxis("Vertical");



            if (player)
            {

                if (v >= 0.5)
                {
                    Debug.Log("ENTER DOOR");

                    if (!map.changeArea)
                        map.ChangeArea(destination);
                }

            }
        }
	}



    void OnTriggerEnter2D(Collider2D c)
    {
        if(c.CompareTag("Player"))
        {
            player = true;
        }
    }

    void OnTriggerStay2D(Collider2D c)
    {
        //if (c.CompareTag("Player"))
        //{
        //    player = true;
        //}
    }

    void OnTriggerExit2D(Collider2D c)
    {
        if (c.CompareTag("Player"))
        {
            player = false;
        }
    }

    

}
