﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class DuruManager : MonoBehaviour {


    Sensor body;

    public int duruNo = 0;


    Text t;

    AudioSource duruFx;

    AudioClip ds;

	void Awake() {

        body = transform.FindChild("BodyCheck").GetComponent<Sensor>();
        t = GameObject.FindGameObjectWithTag("DuruNo").GetComponent<Text>();
        duruFx = GetComponent<AudioSource>();
        //ds = GetComponent<AudioClip>();
    
    }
	
	// Update is called once per frame
	void Update () {
        

        if(body.detect&&body.collideData.gameObject.CompareTag("Duru1"))
        {

            try
            {
                Debug.Log("DURU 1 Detected!");
                Collider2D tmp = body.collideData;
                body.collideData = null;
                Destroy(tmp.gameObject);
                body.detect = false;
                duruNo++;
                t.text = "Duru: " + duruNo;

                //duruFx.PlayOneShot(ds);

               // if(!duruFx.isPlaying)
                //{
                    duruFx.PlayOneShot(duruFx.clip);
               // }
                
                
            }
            catch (Exception e) { }
            }

        t.text = "Duru: " + duruNo;
	}
}
