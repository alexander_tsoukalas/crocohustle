﻿using UnityEngine;
using System.Collections;

public class WeightPlat : MonoBehaviour {


    public GameObject limit;

    public Vector2 origPos;
    public Vector2 destPos;

    bool move = false;
    bool weight = false;
    bool fat = false;




	void Awake () {

        origPos = transform.parent.position;


        if(limit!=null)
        {
            destPos = limit.transform.position;
        }

	}
	

    void Update()
    {
        if(fat)
        {
            Vector2 tmp = transform.parent.position;
            Vector2 sum = tmp - destPos;
            //Debug.Log("SQRMAG F=" + sum.sqrMagnitude);
            if(sum.sqrMagnitude>0.001f)
            {
                move = true;
            }
            else
            {
                move = false;
            }
        }
        else
        {
            Vector2 tmp = transform.parent.position;
            Vector2 sum = tmp - origPos;
           // Debug.Log("SQRMAG NF=" + sum.sqrMagnitude);
            if (sum.sqrMagnitude > 0.001f)
            {
                move = true;
            }
            else
            {
                move = false;
            }
        }

    }


	// Update is called once per frame
	void FixedUpdate () {
	
        if(move)
        {
            if(fat)
            {
                Vector2 tmp = transform.parent.position;

                tmp.y = Mathf.Clamp(tmp.y - 0.05f, destPos.y, origPos.y);
                transform.parent.position = tmp;

            }
            else
            {
                Vector2 tmp = transform.parent.position;

                tmp.y = Mathf.Clamp(tmp.y + 0.05f, destPos.y, origPos.y);
                transform.parent.position = tmp;
            }
        }









        //if(fall)
        //{
        //    Vector2 tmp = transform.parent.position;

        //    tmp.y = Mathf.Clamp(tmp.y - 0.05f, destPos.y, origPos.y);



        //    Vector2 sum = tmp - destPos;

            


        //    Debug.Log("SQRMAG="+sum.sqrMagnitude);





        //    transform.parent.position = tmp;

        //    weight = true;
           




        //}
        //else
        //{

        //    if(weight)
        //    {
        //        Vector2 tmp = transform.parent.position;

        //        tmp.y = Mathf.Clamp(tmp.y + 0.05f, destPos.y, origPos.y);
        //        Vector2 sum = tmp - destPos;
        //        Debug.Log("SQRMAG=" + sum.sqrMagnitude);
        //        transform.parent.position = tmp;

        //    }
        //    //Debug.Log("EST1");
        //}


	}

    void OnCollisionExit2D(Collision2D c)
    {
        if (c.gameObject.CompareTag("NPC"))
        {
            if (c.gameObject.GetComponent<EffectManager>().currentState is FX_FAT)
            {
                fat = false;

            }
        }

    }
    

    void OnCollisionEnter2D(Collision2D c)
    {
        if(c.gameObject.CompareTag("NPC"))
        {
          //  Debug.Log("DO SOMETHING");
        }
    }


    void OnCollisionStay2D(Collision2D c)
    {
        if (c.gameObject.CompareTag("NPC"))
        {
            if(c.gameObject.GetComponent<EffectManager>().currentState is FX_FAT)
            {
                fat = true;
            }
            else
            {
               // Debug.Log("NO FAT ON PLAT");
                fat = false;
            }
        }
    }

}
