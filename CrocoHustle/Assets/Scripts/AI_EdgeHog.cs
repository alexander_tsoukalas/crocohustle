﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MoveController))]
[RequireComponent(typeof(EffectManager))]
public class AI_EdgeHog : MonoBehaviour {

	// Use this for initialization


    GameObject sight;

    public string debugAI;

    public float dirX;
    float dirY;
    bool jump=false;
    bool dash = false;
    public float face = 1;

    Transform sideManage;


    float lastFace;

    float waitTime = 0;

    public Vector2 rayDir;
    Vector2 rayOffset1;
    Vector2 rayOffset2;

    MoveController controller;
    EffectManager effects;

    public delegate void aiState();
    public aiState action;


    bool playerDetect = false;
    public bool falling = false;


    Sensor edgeCheck;

    bool move = true;


	void Awake() {
        sideManage = transform.FindChild("SideManager");
        action = Patrol;
        sight = GameObject.FindGameObjectWithTag("Sight");
        edgeCheck = GameObject.FindGameObjectWithTag("EdgeTest").GetComponent<Sensor>();

        controller = GetComponent<MoveController>();
        effects = GetComponent<EffectManager>();
        
        rayOffset1 = new Vector2(0.1f, 0.1f);
        rayOffset2 = new Vector2(0.2f, 0.2f);
	}
	
	// Update is called once per frame
	void Update () {

        LinesOfSight();

        if (GetComponent<Rigidbody2D>().velocity.y < -1)
            falling = true;
        else
            falling = false;

       
        if(effects.currentState is FX_FAT)
        {
            move = false;
        }
        else
        {
            move = true;
        }




        if(move)
        {
            action();
        }
        

      



        //Vector2 dir = Vector2.right;

        //Vector2 dir1 = dir - new Vector2(0.1f, 0.1f);

        //Vector2 dir2 = dir + new Vector2(0.1f, 0.1f);

        //Vector2 dir3 = dir + new Vector2(0.2f, 0.2f);

        //Vector2 dir4 = dir - new Vector2(0.4f, 0.4f);



        //dir1.Normalize();
        //dir2.Normalize();
        //dir3.Normalize();
        //dir4.Normalize();


        //Debug.DrawRay(sight.transform.position, Vector2.right*10);

        //Debug.DrawRay(sight.transform.position, dir1 * 10);
        //Debug.DrawRay(sight.transform.position, dir2 * 10);
        //Debug.DrawRay(sight.transform.position, dir3 * 10);
        //Debug.DrawRay(sight.transform.position, dir4 * 10);
	}


    void FixedUpdate()
    {
        if (controller.action != null && !controller.inputLock)
        {

            if(move)
            {
                controller.action(dirX, dirY, jump, dash);
            }
            
        }
    }



    void Patrol()
    {
        debugAI = "PATROL";

        if(edgeCheck.detect)
        {
            dirX = face;
        }
        else
        {
            Debug.Log("GO WAIT");
            dirX = 0;
            lastFace = face;
            action = Wait;
        }


      //if(!falling)
      //{
      //    if(edgeCheck.detect)
      //    {
      //        dirX = face;
      //        Debug.Log("WALK PLATFORM");

      //    }
      //    else
      //    {
      //        Debug.Log("CHANGE DIR!");

      //        lastFace=face;
                            
      //        dirX = 0;
      //        action = Wait;

      //    }
      //}

    }


    void LinesOfSight()
    {
        RaycastHit2D hit = Physics2D.Raycast(sight.transform.position, new Vector2(face,0), 10, 1 << LayerMask.NameToLayer("Character"));

        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("Player"))
            {
                Debug.Log("PLAYER DETECTED!");
            }
        }

        Debug.DrawRay(sight.transform.position, new Vector2(face, 0) * 10);
    }

    void Wait()
    {
        debugAI = "WAIT";
        waitTime += Time.deltaTime;

        if(waitTime>=5)
        {



            waitTime = 0;
            face = lastFace * -1;

            if (face == 1)
            {
                sideManage.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            }
            else if (face == -1)
            {
                sideManage.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
            }
            
            action = Patrol;
        }

    }


}
