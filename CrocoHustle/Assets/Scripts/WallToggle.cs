﻿using UnityEngine;
using System.Collections;

public class WallToggle : MonoBehaviour {



    public bool player = false;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {



        GetComponent<Renderer>().enabled = !player;

	}


    void OnTriggerEnter2D(Collider2D c)
    {
        player = true;
    }

    void OnTriggerExit2D(Collider2D c)
    {
        player = false;
    }


}
